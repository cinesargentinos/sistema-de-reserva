
from django.contrib import admin
from django.utils.html import format_html
from django.urls import reverse
from . import models
from . import forms
# Register your models here.

@admin.register(models.EntradasVendidas)
class EntradasVendidasAdmin(admin.ModelAdmin):
	list_display = ('nombre','fecha_desde','fecha_hasta', 'get_agrupar_display', 'get_rollup_display', 'admin_actions')
	form = forms.EntradasVendidasForm

	readonly_fields = ('admin_actions',)

	def admin_actions(self, obj):
		if obj.id:
			return format_html('<a class="button" href="%s">Ver pdf</a>' % reverse('pdf-view', args=[obj.id]))
		return ''

@admin.register(models.Recaudacion)
class RecaudacionAdmin(admin.ModelAdmin):
	list_display = ('nombre', 'fecha_desde', 'fecha_hasta', 'get_agrupar_display', 'get_rollup_display', 'admin_actions')
	form = forms.RecaudacionForm

	readonly_fields = ('admin_actions',)

	def admin_actions(self, obj):
		if obj.id:
			return format_html('<a class="button" href="%s">Ver pdf</a>' % reverse('pdf-view', args=[obj.id]))
		return ''
