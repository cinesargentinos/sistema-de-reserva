
from .models import EntradasVendidas, Recaudacion
from django import forms

class EntradasVendidasForm(forms.ModelForm):
	class Meta:
		model = EntradasVendidas
		fields = '__all__'
		widgets = {
			'texto_inicial': forms.Textarea,
			'texto_final': forms.Textarea
		}

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		agrupar = self.fields['agrupar']
		rollup = self.fields['rollup']
		self.fields['agrupar'] = forms.ChoiceField(choices=tuple(
			(k, self.Meta.model.DIMENSION_NAMES[k])
			for _, k in self.Meta.model.ROUTES
		), required=True, label=agrupar.label)

		self.fields['rollup'] = forms.ChoiceField(choices=
		[('', '------')]+[
			(k2, self.Meta.model.DIMENSION_NAMES[k2])
			for k1,k2 in self.Meta.model.ROUTES.keys()
			if k1 is not None
		], required=False, label=rollup.label)

class RecaudacionForm(EntradasVendidasForm):
	class Meta:
		model = Recaudacion
		fields = '__all__'
		widgets = {
			'texto_inicial': forms.Textarea,
			'texto_final': forms.Textarea
		}
