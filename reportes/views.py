
from statistics import mean

from django.shortcuts import render,get_object_or_404
from django.views import View
from django.utils import timezone
from django.http import HttpResponse
from django.utils import formats
from django.db.models.functions import ExtractWeekDay
from django.db.models import Avg, Max, Count, Min, Sum
from django.http import Http404

from .models import BaseCubeReport, EntradasVendidas, Recaudacion

# Para listado de peliculas (probando la libreria)
from reportlab.platypus import SimpleDocTemplate, Paragraph, TableStyle, Table
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib import colors
from reportlab.lib.enums import TA_CENTER, TA_LEFT
from reportlab.lib.pagesizes import A4, inch
from reportlab.pdfgen import canvas
from reportlab.graphics.shapes import Drawing
from reportlab.graphics.charts.barcharts import HorizontalBarChart, VerticalBarChart
from reportlab.graphics.charts.piecharts import Pie
from reportlab.graphics.charts.textlabels import Label

def sort_function(obj):
	try:
		return obj.id
	except:
		return obj

class ReportView(View):
	style_title = ParagraphStyle(name='Title', fontSize=30, leading=48, alignment=TA_CENTER)
	style_legend = ParagraphStyle(name='Legend', fontSize=8, leading=16, spaceAfter=24, alignment=TA_LEFT)
	style_normal = ParagraphStyle(name='Normal', fontSize=12, leading=16, spaceAfter=28, alignment=TA_LEFT)

	def aggregate(self, ag, iterable):
		if ag == BaseCubeReport.SUM:
			return sum(iterable)
		elif ag == BaseCubeReport.AVG:
			return mean(iterable)
		elif ag == BaseCubeReport.MIN:
			return min(iterable)
		elif ag == BaseCubeReport.MAX:
			return max(iterable)
		elif ag == BaseCubeReport.CNT:
			return sum(1 for _ in iterable)

	def get_groups(self, report):
		grouped = {}
		for ee in report.get_facts().all():
			group_key = report.browse(None, report.agrupar, ee)
			try:
				if isinstance(group_key, str):
					raise TypeError()
				for subkey in group_key:
					grouped[subkey] = grouped.get(subkey, 0) + report.get_fact_value(ee)
			except TypeError:
				grouped[group_key] = grouped.get(group_key, 0) + report.get_fact_value(ee)
		if not report.rollup:
			return grouped
		rolled = {}
		for group_key, group_count in grouped.items():
			new_key = report.browse(report.agrupar, report.rollup, group_key)
			try:
				if isinstance(new_key, str):
					raise TypeError()
				for subkey in new_key:
					rolled[subkey] = rolled.get(subkey, []) + [group_count]
			except TypeError as t:
				rolled[new_key] = rolled.get(new_key, []) + [group_count]
		return {
			k: self.aggregate(report.agregacion, v)
			for k, v in rolled.items()
		}

	def get_table(self, report, aggregated):
		title1 = report.get_rollup_display() if report.rollup else report.get_agrupar_display()
		if report.agregacion:
			title2 = '%s (%s)' % (report.get_fact_name(True), report.get_agregacion_display())
		else:
			title2 = '%s' % report.get_fact_name(True)
		tabledata = [('#', title1, title2)] + [
			(n+1,k,v) for n, (k, v) in enumerate(aggregated)
		]
		return Table(tabledata, style=TableStyle([
			('INNERGRID', (0,0), (-1,-1), 0.25, colors.black),
			('BOX', (0,0), (-1,-1), 0.25, colors.black),
		]))
	def get_horizontal_bar_chart(self, report, aggregated):
		drawing = Drawing()
		barchart = HorizontalBarChart()
		barchart.data = [[v for k,v in aggregated]]
		barchart.valueAxis.valueMin = 0
		barchart.categoryAxis.categoryNames = [str(n+1) for n,(k,v) in enumerate(aggregated)]
		barchart.barWidth = 9
		barchart.groupSpacing = 1
		barchart.width = 5.5*inch

		xlab_str = report.get_rollup_display() if report.rollup else report.get_agrupar_display()
		xlab = Label()
		xlab.setText('# de %s' % xlab_str)
		xlab.angle = 90
		xlab.dy = 50
		xlab.dx = -10

		ylab = Label()
		if report.agregacion:
			ylab.setText('%s (%s)' % (report.get_fact_name(True), report.get_agregacion_display()))
		else:
			ylab.setText('%s' % report.get_fact_name(True))
		ylab.dx = 210
		ylab.dy = -20

		drawing.add(barchart)
		drawing.add(xlab)
		drawing.add(ylab)
		return drawing
	def get_vertical_bar_chart(self, report, aggregated):
		drawing = Drawing()
		barchart = VerticalBarChart()
		barchart.data = [[v for k,v in aggregated]]
		barchart.valueAxis.valueMin = 0
		barchart.categoryAxis.categoryNames = [str(n+1) for n,(k,v) in enumerate(aggregated)]
		barchart.barWidth = 9
		barchart.groupSpacing = 1
		barchart.width = 5.5*inch

		xlab_str = report.get_rollup_display() if report.rollup else report.get_agrupar_display()
		xlab = Label()
		xlab.setText('# de %s' % xlab_str)
		xlab.dx = 210
		xlab.dy = -20

		ylab = Label()
		if report.agregacion:
			ylab.setText('%s (%s)' % (report.get_fact_name(True), report.get_agregacion_display()))
		else:
			ylab.setText('%s' % report.get_fact_name(True))
		ylab.angle = 90
		ylab.dy = 50
		ylab.dx = -10

		drawing.add(barchart)
		drawing.add(xlab)
		drawing.add(ylab)
		return drawing
	def get_pie_chart(self, aggregated):
		drawing = Drawing(4*inch, 4*inch)
		piechart = Pie()
		piechart.width = 4*inch
		piechart.height = 4*inch
		piechart.data = [v for k,v in aggregated]
		try:
			max_len = max(len(k) for n,(k,v) in enumerate(aggregated))
		except:
			max_len = 0
		if (max_len > 25):
			piechart.labels = [str(n+1) for n,(k,v) in enumerate(aggregated)]
		else:
			piechart.labels = [k for n,(k,v) in enumerate(aggregated)]
		drawing.add(piechart)
		return drawing

	def get(self, request, id_reporte):
		for report_class in (EntradasVendidas, Recaudacion):
			try:
				reporte = report_class.objects.get(pk=id_reporte)
				break
			except:
				pass

		if not reporte:
			raise Http404('<h1>Reporte no encontrado</h1>')

		aggregated = self.get_groups(reporte)
		aggregated = [
			(str(k) if k is not None else '-', aggregated[k])
			for k in sorted(aggregated.keys(), key=sort_function)
		]

		response = HttpResponse(content_type='application/pdf')
		response['Content-Disposition'] = 'attachment; filename="%s.pdf"' % reporte.nombre

		doc = SimpleDocTemplate(response, pagesize=A4)

		elements = []
		elements.append(Paragraph('<b>%s</b>' % reporte.nombre, self.style_title))
		elements.append(Paragraph(
				'<i>Reporte generado el %s' % formats.date_format(timezone.now())
				+ '<br/>Contabilizando %s desde el %s hasta el %s' % (reporte.get_fact_name(True), formats.date_format(reporte.fecha_desde), formats.date_format(reporte.fecha_hasta))
				+ '<br/>Agrupando datos por: %s' % reporte.get_agrupar_display()
				+ ('<br/>Haciendo roll-up hasta: %s' % reporte.get_rollup_display() if reporte.rollup else '')
				+ '</i>',
				self.style_legend,
		))

		if reporte.texto_inicial:
			elements.append(Paragraph(reporte.texto_inicial, self.style_normal))

		if reporte.gen_tabla:
			elements.append(self.get_table(reporte, aggregated))
		if reporte.gen_barras_h and len(aggregated) > 0:
			elements.append(self.get_horizontal_bar_chart(reporte, aggregated))
		if reporte.gen_barras_v and len(aggregated) > 0:
			elements.append(self.get_vertical_bar_chart(reporte, aggregated))
		if reporte.gen_torta and len(aggregated) > 0:
			elements.append(self.get_pie_chart(aggregated))

		if reporte.texto_final:
			elements.append(Paragraph(reporte.texto_final, self.style_normal))

		doc.build(elements)
		return response


'''
# Devuelve una lista de todas las entradas en el estado solicitado
def EntradasEstados(estado):
	entradas = []
	for e in EstadoEntrada.objects.all():
		if e.estado == estado:
			entradas.append(e)
	return entradas


# Recibe todas las entradas que debe agrupar y un rango de fecha
# TODO: Rango de fecha
# devuelve un diccionario con clave= TipoVideo y la cantidad de cada uno
def EntradasVendidasTV(entradas):
	entragas_agrupadas_TV = {}
	cantidad = 0
	for e in entradas:
		obj_funcion = Entrada.objects.get(pk=str(e.entrada)).funcion
		obj_pelicula = obj_funcion.pelicula
		obj_tipo_video = obj_pelicula.tipo_video
		tipovideo = obj_tipo_video.nombre
		if (tipovideo in entragas_agrupadas_TV):
			entragas_agrupadas_TV[tipovideo] =	entragas_agrupadas_TV[tipovideo] + 1
		else:
			entragas_agrupadas_TV[tipovideo] = 1
	return entragas_agrupadas_TV

def some_view(request, id_reporte):
	reporte = get_object_or_404(EntradasVendidas, pk=id_reporte)
	response = HttpResponse(content_type='application/pdf')
	#response['Content-Disposition'] = 'attachment; filename="somefilename.pdf"'

	e = EntradasVendidas.objects.get(pk=id_reporte)
	nombre_reporte = e.nombre
	fd_reporte = e.fecha_desde
	fh_reporte = e.fecha_hasta
	criterio_reporte = e.agregar

	entradas = []
	entradas = EntradasEstados('P')

	if criterio_reporte == 'TV':
		agrupadas = {}
		agrupadas = EntradasVendidasTV(entradas)

	p = canvas.Canvas(response)

	x = 30
	y = 700

	for a in agrupadas:
		mostrar = a + str(agrupadas[a])
		p.drawString(x, y, mostrar)
		y = y - 20

	p.showPage()
	p.save()
	#return response


	# Grafico
	buff = BytesIO()
	doc = SimpleDocTemplate(buff,
						pagesize=letter,
						rightMargin=40,
						leftMargin=40,
						topMargin=60,
						bottomMargin=18,
						)
	grafico = []
	header = Paragraph("Product Inventory")
	grafico.append(header)
	data = [[1,2,3,4,5,6,7,8,9,10,11,12]]
	d = Drawing(300, 200)
	chart = VerticalBarChart()
	chart.width = 260
	chart.height = 160
	chart.x = 20
	chart.y = 10
	chart.data = data
	d.add(chart)
	grafico.append(d)
	doc.build(grafico)
	#d.save(fnRoot='example', formats=['png', 'pdf'])

	response.write(buff.getvalue())
	buff.close()
	return response




# Para listado de peliculas (probando la libreria)
def listado_peliculas_view(View):
	print("Genero PDF")
	response = HttpResponse(content_type='application/pdf')
	pdf_name = "Listado_peliculas.pdf"
	buff = BytesIO()
	doc = SimpleDocTemplate(buff,
						pagesize=letter,
						rightMargin=40,
						leftMargin=40,
						topMargin=60,
						bottomMargin=18,
						)
	peliculas = []
	styles = getSampleStyleSheet()
	header = Paragraph("Listado de Peliculas", styles['Heading1'])
	# Agrego el titulo
	peliculas.append(header)
	headings = ('titulo_original', 'titulo')
	allpeliculas = [(p.titulo_original, p.titulo) for p in Pelicula.objects.all()]
	print(allpeliculas)
	t = Table([headings] + allpeliculas)
	t.setStyle(TableStyle(
		[
			('GRID', (0, 0), (3, -1), 1, colors.dodgerblue),
			('LINEBELOW', (0, 0), (-1, 0), 2, colors.darkblue),
			('BACKGROUND', (0, 0), (-1, 0), colors.dodgerblue)
		]
	))
	peliculas.append(t)
	doc.build(peliculas)
	response.write(buff.getvalue())
	buff.close()
	return response
'''
