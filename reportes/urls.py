from django.conf.urls import include, url

from . import views

urlpatterns = [
	url(r'^pdf/(?P<id_reporte>[\d]+)$', views.ReportView.as_view(), name='pdf-view')
]
