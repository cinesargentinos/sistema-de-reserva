from django.contrib import admin
from . import models
from django_admin_listfilter_dropdown.filters import DropdownFilter, RelatedDropdownFilter


@admin.register(models.Pago)
class PagoAdmin(admin.ModelAdmin):
	list_display = ('tarjeta','monto')
	ordering = ['tarjeta']
	search_fields = ('tarjeta',)
	readonly_fields = ('tarjeta', 'monto')
	def has_delete_permission(self, request, obj=None):
		return False
	def has_add_permission(self, request, obj=None):
		return False


@admin.register(models.Tarjeta)
class TarjetaAdmin(admin.ModelAdmin):
	list_display = ('nombre','tipo_tarjeta','banco','habilitada')
	ordering = ['nombre']
	search_fields = ('nombre',)
	list_filter = (
		('tipo_tarjeta',RelatedDropdownFilter),
		('banco',RelatedDropdownFilter),)



@admin.register(models.TipoTarjeta)
class TipoTarjetaAdmin(admin.ModelAdmin):
	list_display = ('nombre','descripcion')
	ordering = ['nombre']
	search_fields = ('nombre',)


@admin.register(models.Banco)
class BancoAdmin(admin.ModelAdmin):
	list_display = ('nombre',)
	ordering = ['nombre']
	search_fields = ('nombre',)


@admin.register(models.EstadoEntrada)
class EstadoEntradaAdmin(admin.ModelAdmin):
	list_display = ('get_pelicula', 'get_sala','estado', 'fecha','get_codigo','pago','usuario','precio')
	ordering = ['entrada']
	list_filter = ('estado',('fecha', admin.DateFieldListFilter),)
	search_fields = ('entrada__id',)
	list_per_page = 30
	readonly_fields = ('entrada', 'estado', 'fecha', 'codigo', 'pago', 'usuario', 'precio')
	def has_add_permission(self, request, obj=None):
		return False

	def get_pelicula(self, obj):
		return obj.entrada.funcion.pelicula
	get_pelicula.short_description = 'Pelicula'
	def get_sala(self, obj):
		return obj.entrada.funcion.sala
	get_sala.short_description = 'Sala'
	def get_codigo(self, obj):
		return obj.codigo[0:8]+'...' if obj.codigo else '-'
