
from django import forms
from django.core.exceptions import ObjectDoesNotExist

from .models import Tarjeta, EstadoEntrada
from simulador.models import Cuenta

class TarjetaForm(forms.Form):
	numero = forms.CharField(label='Número de tarjeta', min_length=9, max_length=16, required=True)
	vencimiento = forms.DateField(label='Vencimiento', required=True, widget=forms.DateInput({'class':'datepicker'}))
	tarjeta = forms.ModelChoiceField(queryset=Tarjeta.objects.filter(habilitada=True).all())
	def clean(self):
		cleaned_data = super(TarjetaForm, self).clean()
		try:
			c = Cuenta.objects.get(
				numero_cuenta=cleaned_data.get('numero'),
				vencimiento=cleaned_data.get('vencimiento')
			)
			return cleaned_data
			if c.saldo() < 0:
				raise forms.ValidationError('La tarjeta seleccionada es inválida')
		except ObjectDoesNotExist:
			raise forms.ValidationError('No se pudo confirmar los datos de la tarjeta')

class TarjetaOrCashForm(forms.Form):
	pay_cash = forms.BooleanField(label='Pago en efectivo', required=False)
	numero = forms.CharField(label='Número de tarjeta', min_length=9, max_length=16, required=False)
	vencimiento = forms.DateField(label='Vencimiento', widget=forms.DateInput({'class':'datepicker'}), required=False)
	tarjeta = forms.ModelChoiceField(queryset=Tarjeta.objects.filter(habilitada=True).all(), required=False)

	def clean(self):
		cleaned_data = super().clean()
		if cleaned_data['pay_cash']:
			cleaned_data['numero'] = None
			cleaned_data['vencimiento'] = None
			cleaned_data['tarjeta'] = None
			return cleaned_data

		for field in ('numero', 'vencimiento', 'tarjeta'):
			if not cleaned_data.get(field):
				self.add_error(field, forms.ValidationError('Este campo es requerido para el pago con tarjeta'))
		if not all(self.cleaned_data.get('tarjeta') for field in ('numero', 'vencimiento', 'tarjeta')):
			raise forms.ValidationError('Datos de la tarjeta incorrectos')
		try:
			c = Cuenta.objects.get(
				numero_cuenta=cleaned_data.get('numero'),
				vencimiento=cleaned_data.get('vencimiento')
			)
		except ObjectDoesNotExist:
			raise forms.ValidationError('No se pudo confirmar los datos de la tarjeta')
		if c.saldo() < 0:
			raise forms.ValidationError('La tarjeta seleccionada es inválida')
		return cleaned_data


class CodeForm(forms.Form):
	codigo = forms.CharField(label='Código', min_length=26, max_length=64, required=True)
	def clean(self):
		cleaned_data = super().clean()
		ees = EstadoEntrada.objects.filter(codigo=cleaned_data['codigo']).all()
		ees = [ee for ee in ees if ee.is_newest()]
		if not ees:
			raise forms.ValidationError('El codigo no se corresponde con ninguna entrada')
		return cleaned_data
