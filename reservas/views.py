import hashlib
import barcode
import math
import io

from django.template.response import TemplateResponse
from django.views import View
from django.shortcuts import render,redirect,get_list_or_404,get_object_or_404
from django.urls import reverse, resolve
from django.utils import timezone
from django.core.serializers import serialize

from .models import Funcion,Precio
from .forms import TarjetaForm, TarjetaOrCashForm, CodeForm
from reservas.models import Entrada, EstadoEntrada, Pago
from usuarios.views import ReservationsView
from usuarios.models import User
from peliculas.views import VerPeliculaView
from sistema_de_reserva import settings

def get_price_context_for_function(funcion):
	return {
		'prices': serialize('json', funcion.precios_vigentes()),
		'pricetypes': serialize('json', {p.tipo_precio for p in funcion.precios_vigentes()})
	}
def get_context_for_map(funcion):
	ctx = {
		'room': serialize('json', [funcion.sala]),
		'seats': serialize('json', funcion.sala.butaca_set.all()),
		'entries': serialize('json', funcion.entrada_set.all()),
		'entrystates': serialize('json', (e.estadoentrada_current() for e in funcion.entrada_set.all())),
	}
	ctx.update(get_price_context_for_function(funcion))
	return ctx
def get_code_svg(code):
	Code39 = barcode.get_barcode_class('code39')
	code39 = Code39(code, add_checksum=False)
	return code39.render({})

def get_codigo(funcion, user):
	digest = hashlib.md5((str(timezone.now()) + str(user.id)).encode('utf8')).digest()
	charset = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'
	charset_bits = math.floor(math.log(len(charset),2))
	str_digest_bits = math.ceil(len(digest)*8/charset_bits)*charset_bits
	digest = ''.join(bin(x)[2:].zfill(8) for x in digest).zfill(str_digest_bits)
	digits = [int(digest[char0*5:char0*5+5],2) for char0 in range(0, str_digest_bits//5)]
	return ''.join(charset[d] for d in digits)

class ReserveView(View):
	"""
	Vista desde la cual el usuario final reserva entradas
	"""
	def breadcrumbs(id_funcion):
		f = Funcion.objects.get(pk=id_funcion)
		lt = timezone.localtime(f.fecha)
		return VerPeliculaView.breadcrumbs(f.pelicula.id) + [{
			'name': 'El %s a las %s en %s' % (f.fecha.date(), f.fecha.time(), f.sala),
			'url': reverse('reservas-reserve', args=[id_funcion])
		}]
	def post(self, request, id_funcion, *args, **kwargs):
		funcion = get_object_or_404(Funcion, pk=id_funcion)
		codigo = get_codigo(funcion, request.user)
		estadoentradas = [EstadoEntrada.objects.get(pk=eeid) for eeid in request.POST.getlist('entradas')]
		entradas = [ee.entrada for ee in estadoentradas]
		if all(e.reservable() for e in entradas):
			for entrada in entradas:
				entrada.reservar(usuario=request.user, codigo=codigo)
			return ReservationsView().get(request)
		else:
			return self.get(request, id_funcion)

	def get(self, request, id_funcion, *args, **kwargs):
		funcion = get_object_or_404(Funcion, pk=id_funcion)
		pendientes = 0
		if (request.user.is_authenticated()):
			pendientes = EstadoEntrada.objects.filter(usuario=request.user, estado=EstadoEntrada.R)
			pendientes = sum(1 for p in pendientes if p.is_newest())
		if not request.user.is_authenticated():
			max_reserva = 0
		elif request.user.limite_reserva is not None:
			max_reserva = request.user.limite_reserva.max_reserva
		else:
			max_reserva = -1
		ctx = {
			'funcion': funcion,
			'max_reserva': max_reserva,
			'reservas_pendientes': pendientes,
			'breadcrumbs': ReserveView.breadcrumbs(id_funcion),
			'date_input_format': settings.DATE_INPUT_FORMAT_JQUERY,
		}
		ctx.update(get_context_for_map(funcion))
		return TemplateResponse(request, 'reservas/reservar-funcion.html', ctx)

class BuyingView(View):
	"""
	Vista desde la cual el usuario final compra entradas
	"""
	def breadcrumbs(id_funcion):
		f = Funcion.objects.get(pk=id_funcion)
		lt = timezone.localtime(f.fecha)
		return VerPeliculaView.breadcrumbs(f.pelicula.id) + [{
			'name': 'El %s a las %s en %s' % (f.fecha.date(), f.fecha.time(), f.sala),
			'url': reverse('reservas-buy', args=[id_funcion])
		}]
	def post(self, request, id_funcion, *args, **kwargs):
		funcion = get_object_or_404(Funcion, pk=id_funcion)
		estadoentradas = [EstadoEntrada.objects.get(pk=eeid) for eeid in request.POST.getlist('entradas')]
		entradas = [ee.entrada for ee in estadoentradas]
		precios = request.POST.getlist('precio')
		precios = [Precio.objects.get(pk=p_id) for p_id in precios]
		tarjeta = TarjetaForm(request.POST)
		if tarjeta.is_valid() and all(e.vendible(request.user) for e in entradas):
			monto = sum(p.monto for p in precios)
			codigo = get_codigo(funcion, request.user)
			pago = Pago.objects.create(tarjeta=tarjeta.cleaned_data['tarjeta'], monto=monto)
			pago.save()
			for e,p in zip(entradas, precios):
				e.vender(request.user, codigo, p, pago)
			return redirect('accounts-reservations')
		else:
			return self.get(request, id_funcion, tarjeta_form=tarjeta)
	def get(self, request, id_funcion, *args, **kwargs):
		funcion = get_object_or_404(Funcion, pk=id_funcion)
		ctx = {
			'funcion': funcion,
			'breadcrumbs': BuyingView.breadcrumbs(id_funcion),
			'tarjeta_form': kwargs.get('tarjeta_form', TarjetaForm()),
			'date_input_format': settings.DATE_INPUT_FORMAT_JQUERY,
		}
		ctx.update(get_context_for_map(funcion))
		return render(request, 'reservas/comprar-funcion.html', ctx)

class SellingView(View):
	"""
	Vista desde la cual el operador vende entradas
	"""
	def breadcrumbs(id_funcion):
		f = Funcion.objects.get(pk=id_funcion)
		lt = timezone.localtime(f.fecha)
		return VerPeliculaView.breadcrumbs(f.pelicula.id) + [{
			'name': 'El %s a las %s en %s' % (f.fecha.date(), f.fecha.time(), f.sala),
			'url': reverse('reservas-buy', args=[id_funcion])
		}]
	def post(self, request, id_funcion, buying_user=None, *args, **kwargs):
		if not request.user.is_staff:
			return redirect('reservas-buy', id_funcion)
		funcion = get_object_or_404(Funcion, pk=id_funcion)
		estadoentradas = [EstadoEntrada.objects.get(pk=eeid) for eeid in request.POST.getlist('entradas')]
		entradas = [ee.entrada for ee in estadoentradas]
		precios = request.POST.getlist('precio')
		precios = [Precio.objects.get(pk=p_id) for p_id in precios]
		tarjeta = TarjetaOrCashForm(request.POST)
		if buying_user is not None:
			buying_user = User.objects.get(pk=buying_user)
		if tarjeta.is_valid():
			if all(e.vendible(buying_user) for e in entradas):
				monto = sum(p.monto for p in precios)
				codigo = get_codigo(funcion, request.user)
				t = tarjeta.cleaned_data['tarjeta']
				pago = Pago.objects.create(tarjeta=t, monto=monto)
				pago.save()
				for e,p in zip(entradas, precios):
					e.vender(buying_user, codigo, p, pago)
				return self.get(request, id_funcion, buying_user.id if buying_user is not None else None, last_code=codigo)
			else:
				return self.get(request, id_funcion, buying_user.id if buying_user is not None else None, tarjeta_form=tarjeta)
		else:
			return self.get(request, id_funcion, buying_user, tarjeta_form=tarjeta)
	def get(self, request, id_funcion, buying_user=None, *args, **kwargs):
		if not request.user.is_staff:
			return redirect('reservas-buy', id_funcion)
		funcion = get_object_or_404(Funcion, pk=id_funcion)
		context = {
			'funcion': funcion,
			'breadcrumbs': BuyingView.breadcrumbs(id_funcion),
			'tarjeta_form': kwargs.get('tarjeta_form', TarjetaOrCashForm()),
			'date_input_format': settings.DATE_INPUT_FORMAT_JQUERY,
			'last_code': kwargs.get('last_code', None),
			'buying_user': User.objects.get(pk=buying_user) if buying_user is not None else None,
		}
		context.update(get_context_for_map(funcion))
		return render(request, 'reservas/vender-funcion.html', context)

class CodeView(View):
	template_file = 'reservas/ver-codigo.html'

	def post(self, request, codigo):
		precios = request.POST.getlist('precio')
		precios = [Precio.objects.get(pk=precio_id) for precio_id in precios]
		return self.get(request, codigo, precios)

	def get(self, request, codigo, precios=None):
		estadoentradas = get_list_or_404(EstadoEntrada, codigo=codigo)
		estadoentradas = [e for e in estadoentradas if e.is_newest()]
		entradas = [ee.entrada for ee in estadoentradas]
		butacas = [e.butaca for e in entradas]
		funcion = estadoentradas[0].entrada.funcion
		user = estadoentradas[0].usuario
		if request.user != user and not request.user.is_staff:
			return redirect(reverse('accounts-reservations'))
		precios = precios or list(ee.precio for ee in estadoentradas)
		ctx = {
			'funcion': funcion,
			'user': user,
			'code_plain': codigo,
			'code39': get_code_svg(codigo),
			'estadoentradas': list(zip(estadoentradas, precios)),
			'subtotal': sum(p.monto for p in precios if p is not None),
			'allow_sell': all(e.estado==EstadoEntrada.R for e in estadoentradas),
			'date_input_format': settings.DATE_INPUT_FORMAT_JQUERY,
			'entrystates': serialize('json', estadoentradas),
			'entries': serialize('json', entradas),
			'seats': serialize('json', butacas),
			'rooms': serialize('json', [funcion.sala]),
		}
		ctx.update(get_price_context_for_function(funcion))
		return TemplateResponse(request, self.template_file, ctx)

class VerifyCodeView(CodeView):
	template_file = 'reservas/verificar-codigo.html'
	def post(self, request, *args, **kwargs):
		if not request.user.is_staff:
			return redirect('/')
		code_form = CodeForm(request.GET)
		tarjeta_form = TarjetaOrCashForm(request.POST)
		if not tarjeta_form.is_valid() or not code_form.is_valid():
			return self.get(request, code_form, tarjeta_form, *args, **kwargs)

		codigo = code_form.cleaned_data['codigo']
		estadoentradas = EstadoEntrada.objects.filter(codigo=codigo)
		estadoentradas = [e for e in estadoentradas if e.is_newest()]
		precios = request.POST.getlist('precio')
		precios = [Precio.objects.get(pk=p_id) for p_id in precios]
		buying_user = estadoentradas[0].usuario

		if all(ee.vendible(buying_user) for ee in estadoentradas):
			monto = sum(p.monto for p in precios)
			pago = Pago.objects.create(tarjeta=tarjeta_form.cleaned_data['tarjeta'], monto=monto)
			pago.save()
			for e,p in zip(estadoentradas, precios):
				e.entrada.vender(buying_user, codigo, p, pago)
		return self.get(request, code_form, tarjeta_form)

	def get(self, request, code_form=None, tarjeta_form=None, *args, **kwargs):
		if request.GET:
			code_form = CodeForm(request.GET)
			tarjeta_form = tarjeta_form or TarjetaOrCashForm()
			if code_form.is_valid():
				codigo = code_form.cleaned_data['codigo']
				tarjeta_form = tarjeta_form or TarjetaOrCashForm()
				response = super().get(request, codigo)
				response.context_data.update(code_form=code_form, tarjeta_form=tarjeta_form)
				return response
		code_form = code_form or CodeForm()
		return render(request, self.template_file, {'code_form': code_form})

class PrintReserveView(View):
	template_file = 'reservas/pagar-reserva.html'
	def breadcrumbs(self, codigo):
		return ReservationsView.breadcrumbs() + [{
			'name': 'Imprimir reserva',
			'url': reverse('reservas-print', args=[codigo]),
		}]
	def post(self, request, codigo):
		return CodeView().post(request, codigo)

	def get(self, request, codigo, *args, **kwargs):
		ees = get_list_or_404(EstadoEntrada, codigo=codigo, usuario=request.user)
		ees = [ee for ee in ees if ee.is_newest()]
		seats = [ee.entrada.butaca for ee in ees]
		if seats:
			funcion = ees[0].entrada.funcion
			ctx = {
				'codigo': codigo,
				'code_plain': codigo,
				'funcion': funcion,
				'seats': serialize('json', seats),
				'breadcrumbs': self.breadcrumbs(codigo),
				'date_input_format': settings.DATE_INPUT_FORMAT_JQUERY,
			}
			ctx.update(get_price_context_for_function(funcion))
			return TemplateResponse(request, self.template_file, ctx)

class PaymentView(PrintReserveView):
	def breadcrumbs(self, codigo):
		return ReservationsView.breadcrumbs() + [{
			'name': 'Pago online',
			'url': reverse('reservas-payment', args=[codigo]),
		}]
	def post(self, request, codigo, *args, **kwargs):
		tarjeta = TarjetaForm(request.POST)
		estadoentradas = get_list_or_404(EstadoEntrada, codigo=codigo)
		estadoentradas = [e for e in estadoentradas if e.is_newest()]
		entradas = [ee.entrada for ee in estadoentradas]
		precios = request.POST.getlist('precio')
		precios = [Precio.objects.get(pk=p_id) for p_id in precios]
		monto = sum(p.monto for p in precios)
		if not tarjeta.is_valid():
			return self.get(request, codigo, tarjeta_form=tarjeta, *args, **kwargs)
		if all(e.vendible(request.user) for e in entradas) and len(entradas) == len(precios):
			pago = Pago.objects.create(tarjeta=tarjeta.cleaned_data['tarjeta'], monto=monto)
			pago.save()
			for e,p in zip(entradas, precios):
				e.vender(request.user, codigo, p, pago)
			return redirect('accounts-reservations')
		else:
			return self.get(request, codigo, tarjeta_form=tarjeta, *args, **kwargs)

	def get(self, request, codigo, tarjeta_form=None, *args, **kwargs):
		response = super().get(request, codigo)
		response.context_data.update(tarjeta_form=tarjeta_form or TarjetaForm())
		return response
