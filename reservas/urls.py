from django.conf.urls import url, include
from . import views

urlpatterns = [
    url(r'reservar/(?P<id_funcion>[0-9]+)$', views.ReserveView.as_view(), name='reservas-reserve'),
	url(r'comprar/(?P<id_funcion>[0-9]+)$', views.BuyingView.as_view(), name='reservas-buy'),
	url(r'vender/(?P<id_funcion>[0-9]+)/(?P<buying_user>[0-9]+)?$', views.SellingView.as_view(), name='reservas-sell'),
    url(r'pagar-codigo/(?P<codigo>[a-zA-Z0-9]+)$', views.PaymentView.as_view(), name='reservas-payment'),
	url(r'imprimir/(?P<codigo>[a-zA-Z0-9]+)$', views.PrintReserveView.as_view(), name='reservas-print'),
	url(r'codigo/(?P<codigo>[a-zA-Z0-9]+)$', views.CodeView.as_view(), name='reservas-code'),
	url(r'verificar/$', views.VerifyCodeView.as_view(), name='reservas-verifycode')
]
