# Sistema de Reservas de Cines Argentinos #


> **Práctica Profesional - Seminario de Integración Profesional**

> **Lic. en Sistemas de Información**

> **Universidad Nacional de Luján**

> **Año 2017**



## Contenido ##

* [Instalación para desarrollo] (#instalaci%C3%B3n-para-desarrolo)
* [Usando git] (#usando-git)
* [Documentación relevante] (#documentaci%C3%B3n-relevante)


- - -

## Instalación para desarrolo ##

### Pre-requisitos

***NOTA:*** *Puede que muchos de los siguientes paquetes ya estén instalados*

* **Cliente git:** ``` $ sudo apt install git ```
* **Python3** ``` $ sudo apt install python3 python3-dev python3-setuptools ```
* **pip3:** ``` $ sudo apt install python3-pip ```
* **Virtualenv:** ``` $ sudo apt install python3-virtualenv ```
* **Postgresql:** (PostgreSQL 9.5.6)``` $ sudo apt install postgresql ```
* **Pillow:** (Para el manejo de imágenes) ``` $ sudo apt install libjpeg-dev ```

_**Nota:**_ _Agregar todo el resto de las dependencias que vean que van saliendo_


### Instalación de proyecto

#### Entorno Virtual de Python

Este paso es optativo pero **recomendado** para desarrollo ya que [virtualenv](https://virtualenv.pypa.io/en/stable/) permite crear un espacio completamente independiente de otros entornos virtuales y de los paquetes instalados globalmente en el sistema.

* Crear un entorno virtual para el proyecto
```bash
    $ virtualenv -p python3 [nombre del entorno]
```
>Donde **[nombre del entorno]** es un nombre a elección para su entorno virtual, este comando crea una carpeta con el misma nombre donde irán todos los paquetes de python que sean utilizados.

* Activar el entorno
```bash
    $ cd [nombre del entorno]
    $ . bin/activate
    ([nombre del entorno])$
```

* Para salir del entorno virtual
```bash
    $ deactivate
```


#### Clonar repositorio

_En caso de estar usando virtualenv, se recomienda realizar el clone dentro de la carpeta del entorno virtual para saber que entorno se esta usando con ese proyecto. (Esto es solo una recomendación)_

```bash
    $ git clone https://gitlab.com/cinesargentinos/sistema-de-reserva
```

Se creará una carpeta con el nombre *sistema-de-reserva*


#### Instalación de dependencias con pip:

```bash
    $ cd sistema-de-reserva
    $ pip3 install -r requirements.txt
```

#### Configuración de la base de datos (PostgresQL):

Primero hay que acceder con el usuario por default de Postgresql:

```bash
    $ sudo -u postgres psql
```

(Los datos para crear la base de datos se encuentran especificados en *sistema_de_reserva/settings.py*)

```sql
    postgres=# CREATE USER cinesargentinos WITH PASSWORD 'jpostel';
    postgres=# ALTER ROLE cinesargentinos SET client_encoding = 'utf8';
    postgres=# ALTER ROLE cinesargentinos SET default_transaction_isolation = 'read committed';
    postgres=# CREATE DATABASE cinesargentinos;
    postgres=# \q

```

#### Instalación de gulp para compilar React+JSX a JavaScript:
```bash
	\# npm install gulp-cli -g
```

#### Instalación de dependencias de desarrollo de JavaScript (React+Babel):
```bash
	$ npm install
```

#### Creacion/Actualizacion del esquema de la base de datos:
```bash
    $ python3 migrar.py
```

#### Creacion de un usuario administrativo:
```bash
    $ python3 manage.py createsuperuser
```

#### Ejecucion del servidor de pruebas (local)
```bash
    $ python3 manage.py runserver
```

#### Reiniciar el servidor Apache (deploy)
```bash
    $ sudo systemctl restart apache2
```


- - -



## Usando git ##


Mostrar estado del repositorio local

```bash
    $ git status
```

![](https://git-scm.com/book/en/v2/images/areas.png)

Agregar cambios a la staging area:
```bash
    $ git add archivo1, archivo2, ..., archivoN
```

Realizar el commit en el repositorio local:
```bash
    $ git commit -m 'mensaje descriptivo de los cambios'
```

Subir el commit del repositorio local al remoto:
```bash
    $ git push origin master
```

Descargar commits del repo remoto al local:
```bash
    $ git pull origin master
```

Listar commits presentes en el repositorio local:
```bash
    $ git log
```

Descartar los cambios en el working directory:
```bash
    $ git reset --head
```

Descartar los cambios de un archivo:
```bash
    $ git checkout path/al/archivo
```

---


### Documentación relevante:
> * [Django 1.10](https://docs.djangoproject.com/en/1.10/)
> * [Django-registration 2.2](http://django-registration.readthedocs.io/en/2.2/index.html)
> * [Virtualenv](https://virtualenv.pypa.io/en/stable/)
