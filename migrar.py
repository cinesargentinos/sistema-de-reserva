#!/usr/bin/env python3

try:
    from django.core.management import execute_from_command_line
except ImportError:
    raise ImportError("No se pudo importar django")

from sistema_de_reserva.settings import INSTALLED_APPS
import re
import sys
from shutil import rmtree
import os

# directorio base del proyecto
basedir = os.path.dirname(sys.argv[0])
managepy = os.path.join(basedir, 'manage.py')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sistema_de_reserva.settings")

django_app_re = re.compile(r'^django\.\w+\.(\w+)$')
local_app_re = re.compile(r'(\w+)\.apps\.\w+')

def get_names(installed, reobj):
    return [m.group(1) for m in map(reobj.match, installed) if m is not None]

django_apps = get_names(INSTALLED_APPS, django_app_re)
local_apps = get_names(INSTALLED_APPS, local_app_re)

for local in local_apps: # eliminar migrations viejas
    rmtree(os.path.join(basedir, local, 'migrations'), ignore_errors=True)
for app in django_apps + local_apps: # realizar migrations nuevas
    execute_from_command_line([managepy, 'makemigrations', app])

execute_from_command_line([managepy, 'migrate'])

