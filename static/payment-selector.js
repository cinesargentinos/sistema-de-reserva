
function PaymentSelector(prices, selects, total_output) {
	let pricesById = {};
	for (let p of prices)
		pricesById[p.id] = p;

	let updateTotal = function() {
		total_output.innerHTML = '$' + selects.reduce(function(a, b) { return a + pricesById[b.value].monto; }, 0);
	}

	let addOptions = function(s) {
		s.innerHTML = '';
		for (p of prices) {
			let option = document.createElement('option');
			option.value = p.id;
			option.appendChild(document.createTextNode(p.str))
			s.appendChild(option);
		}
		s.onchange = updateTotal;
	}
	for (s of selects)
		addOptions(s);

	this.selects = selects;
	this.addSelect = function(select) {
		selects.push(select);
		addOptions(select);
		updateTotal()
	}
	this.removeSelect = function(select) {
		selects.splice(selects.indexOf(select), 1);
		updateTotal();
	}
	this.getSelectedPromotableEntries = function() {
		return selects.reduce(function(a,b) {
			if (!pricesById[b.value].nap)
				a.push(pricesById(b));
			return a;
		}, []);
	}
	updateTotal();
}
