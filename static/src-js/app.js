
let int2letters = function(x, charset) {
	charset = charset || 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	let s = [x % charset.length];
	x = Math.floor(x/charset.length);
	while (x > 0) {
		s.splice(0, 0, Math.max(0, x % (charset.length+1) - 1))
		x = Math.floor(x / (charset.length+1))
	}
	s = s.map(function(idx) { return charset[idx]; });
	return s.join('');
}

class Butaca {
	constructor(data) {
		this.pk = data.pk;
		this.fields = data.fields;
	}
	toString() {
		return int2letters(this.fields.fila-1) + '-' + this.fields.columna
	}
}
class EstadoEntrada {
	constructor(data) {
		this.pk = data.pk;
		this.fields = data.fields;
	}
	get_estado_display() {
		return {
			'R': 'Reservada',
			'L': 'Libre',
			'D': 'Disponible',
			'P': 'Paga'
		}[this.fields.estado];
	}
}
/*Array.prototype.binarySearch = function(compare_to) {
	let left = 0;
	let right = this.length;
	while(left <= right) {
		let pivot_key = Math.floor((left+right)/2)
		let v = compare_to(this[pivot_key]);
		if (v < 0)
			right = pivot_key-1;
		else if (v > 0)
			left = pivot_key+1;
		else
			return this[pivot_key]
	}
	return null;
}*/

function Seat(props) {
	let eState = '\xA0';
	let input = null;
	if (props.entryState.fields.entrada.fields.butaca.fields.disponibilidad != 'V') {
		eState = props.entryState.fields.estado;
		input = <input type="checkbox" name="entradas"
			value={props.entryState.pk} disabled={props.disabled}
			readOnly={props.disabled} onClick={props.onClick} />
	}
	return <td className="entrada">{eState}{input}</td>;
}

class GenericEntryMap extends React.Component {
	constructor(props) {
		super(props);
	}
	renderSeat(i) {
		let es = this.props.room.fields.butaca_set[i].fields.entrada_set[0].fields.estadoentrada_set[0];
		if (!this.props.onSeatClick)
			var onSeatClick = () => {};
		else
			var onSeatClick = ev => this.props.onSeatClick(i, ev, es)
		return (
			<Seat key={i} entryState={es} disabled={this.props.disabledSeats[i]} onClick={onSeatClick} />
		);
	}
	shouldComponentUpdate(nextProps, nextState) {
		return this.props.disabledSeats !== nextProps.disabledSeats;
	}
	render() {
		let room = this.props.room;
		return (
			<table>
				<thead>
					<tr>
						<td></td>
						{[...Array(room.fields.cant_columnas).keys()].map(c => <td key={c}>{c+1}</td>)}
					</tr>
				</thead>
				<tbody>
					{[...Array(room.fields.cant_filas).keys()].map(row =>
						<tr key={row}>
							<td>{int2letters(row)}</td>
							{[...Array(room.fields.cant_columnas).keys()]
									.map(col => this.renderSeat(row * this.props.room.fields.cant_columnas + col))}
						</tr>
					)}
				</tbody>
			</table>
		);
	}
}

function ReserveCounter(props) {
	if (props.reservesLeft < 0)       return <p>Podés reservar todas las entradas que quieras</p>
	else if (props.reservesLeft == 0) return <p>No podés reservar más entradas</p>
	else if (props.reservesLeft == 1) return <p>Podés reservar una entrada más</p>
	else if (props.reservesLeft > 0)  return <p>Podés reservar {props.reservesLeft} entradas más</p>
}

class ReservesMap extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			'reserved': 0,
			'checkedEntryStates': [],
		};
		this.onSeatClick = this.onSeatClick.bind(this);
	}
	onSeatClick (i, ev, entryState) {
		const checkedEntryStates = this.state.checkedEntryStates.slice();
		checkedEntryStates[i] = !checkedEntryStates[i];
		if (checkedEntryStates[i])
			this.setState({'reserved': this.state.reserved+1, 'checkedEntryStates': checkedEntryStates});
		else
			this.setState({'reserved': this.state.reserved-1, 'checkedEntryStates': checkedEntryStates});
	}

	reservesLeft() {
		if (this.props.maxReserves==null || this.props.maxReserves==-1)
			return -1;
		else
			return this.props.maxReserves-this.props.pendingReserves-this.state.reserved;
	}

	isSeatDisabled(i, ev) {
		return (this.props.room.fields.butaca_set[i].fields.entrada_set[0].fields.estadoentrada_set[0].fields.estado != 'D' ||
				this.reservesLeft() == 0) && !this.state.checkedEntryStates[i];
	}

	render() {
		const room_size = this.props.room.fields.cant_filas * this.props.room.fields.cant_columnas;
		return (
			<div>
				<GenericEntryMap
					room={this.props.room}
					onSeatClick={this.onSeatClick}
					disabledSeats={
						[...Array(room_size).keys()].map(i => this.isSeatDisabled(i))
					}
					/>
				<ReserveCounter reservesLeft={this.reservesLeft()} />
			</div>
		);
	}
}

function SeatPricePicker(props) {
	let onChange = ev => props.onChange(ev, props.prices.find(o=>o.pk==ev.target.value));
	return (
		<tr>
			{props.fields.map((f, i) =>
				<td key={i}>{f(props.entryState)}</td>
			)}
			<td><select name="precio" onChange={onChange}>
			{props.prices.map(p =>
				<option key={p.pk} value={p.pk}>{p.fields.tipo_precio.fields.nombre} ${p.fields.monto}</option>
			)}
			</select></td>
		</tr>
	);
}
class SeatPricePickerList extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			'selectedPrices': Array(this.props.entryStates.length).fill(this.props.prices[0])
		}
		this.priceChanged = this.priceChanged.bind(this);
	}
	componentWillReceiveProps(nextProps) {
		let fillValue = this.props.prices[0];
		let [a,b] = [this.state.selectedPrices.length, nextProps.entryStates.length];
		if (a < b)
			this.setState({'selectedPrices': this.state.selectedPrices.concat(Array(b-a).fill(fillValue))});
		else if (b < a)
			this.setState({'selectedPrices': this.state.selectedPrices.slice(0, b)});
	}
	priceChanged(i, ev, price) {
		let selectedPrices = this.state.selectedPrices.slice();
		selectedPrices[i] = price
		this.setState({'selectedPrices': selectedPrices});
	}
	getTotal() {
		return this.state.selectedPrices.reduce((x, p) => x + parseFloat(p.fields.monto), 0);
	}
	render() {
		return (
			<table id="seleccion-precio">
				<thead><tr>
					{this.props.fieldHeads.map((fh,i) => <td key={i}>{fh}</td>)}
					<td>Tipo de Entrada</td>
				</tr></thead>
				<tbody>
					{this.state.selectedPrices.map((p, i) =>
						<SeatPricePicker key={i} entryState={this.props.entryStates[i]}
							prices={this.props.prices} fields={this.props.fields}
							onChange={(ev, p) => this.priceChanged(i, ev, p)} value={p} />
					)}
				</tbody>
				<tfoot><tr>
					{this.props.fields.slice(-2).map(k => <td key={k}></td>)}
					<td>Subtotal</td>
					<td>${this.getTotal()}</td>
				</tr></tfoot>
			</table>
		);
	}
}

class BuyingMap extends React.Component {
	constructor(props) {
		super(props);
		this.state = {'checkedEntryStates': []};
		this.onSeatClick = this.onSeatClick.bind(this);
	}
	onSeatClick (i, ev, entryState) {
		const checkedEntryStates = this.state.checkedEntryStates.slice();
		checkedEntryStates[i] = !checkedEntryStates[i];
		if (checkedEntryStates[i])
			this.setState({'checkedEntryStates': checkedEntryStates});
		else
			this.setState({'checkedEntryStates': checkedEntryStates});
	}
	onSeatClick(i, ev, entryState) {
		let ss = this.state.checkedEntryStates;
		if (ev.target.checked)
			ss = ss.concat([entryState]);
		else
			ss = ss.filter(s => s.pk != entryState.pk);
		this.setState({'checkedEntryStates': ss});
	}

	isSeatDisabled(i) {
		let ee = this.props.room.fields.butaca_set[i].fields.entrada_set[0].fields.estadoentrada_set[0];
		return (ee.fields.estado == 'R' || ee.fields.estado == 'P');
	}
	render() {
		const room_size = this.props.room.fields.cant_filas * this.props.room.fields.cant_columnas;
		return (
			<div>
				<GenericEntryMap
					room={this.props.room}
					onSeatClick={this.onSeatClick}
					disabledSeats={
						[...Array(room_size).keys()].map(i => this.isSeatDisabled(i))
					} />
				<SeatPricePickerList
					fieldHeads={['Butaca']}
					fields={[es => es.fields.entrada.fields.butaca.toString()]}
					entryStates={this.state.checkedEntryStates}
					prices={this.props.prices} />
			</div>
		);
	}
}

/*function StartMap(element, room, seats, entries, entryStates) {
	console.log(element);
	ReactDOM.render(<GenericEntryMap room={room} seats={seats} entries={entries} entryStates={entryStates}  />, element);
}*/

function _preprocess_querysets(props) {
	let relate = function(set1, relationField, set2, relatedField) {
		for (let object of set1) {
			let relatedObject = set2.find(obj => obj.pk == object.fields[relationField]);
			object.fields[relationField] = relatedObject;
			if (relatedField) {
				let rf = relatedObject.fields[relatedField] || [];
				rf.push(object);
				relatedObject.fields[relatedField] = rf;
			}
		}
	}

	if (props.seats) {
		props.seats = props.seats.map(s => new Butaca(s));
		if (props.rooms) {
			let seatSortKey = seat => seat.fields.fila*seat.fields.sala.fields.cant_columnas + seat.fields.columna;
			relate(props.seats, 'sala', props.rooms, 'butaca_set');
			for (let r of props.rooms) {
				r.fields.butaca_set = r.fields.butaca_set.sort((a,b) => seatSortKey(a)-seatSortKey(b))
			}
			props.seats = props.seats.sort((a,b) => seatSortKey(a) - seatSortKey(b));
		}
	}
	if (props.seats && props.entries)
		relate(props.entries, 'butaca', props.seats, 'entrada_set');
	if (props.entryStates) {
		props.entryStates = props.entryStates.map(data => new EstadoEntrada(data));
		if (props.entries)
			relate(props.entryStates, 'entrada', props.entries, 'estadoentrada_set');
	}
	if (props.priceTypes && props.prices)
		relate(props.prices, 'tipo_precio', props.priceTypes, 'precio_set');

	return props;
}

function StartReservesMap(element, props) {
	props = _preprocess_querysets(props);
	ReactDOM.render(<ReservesMap room={props.rooms[0]} maxReserves={props.maxReserves} pendingReserves={props.pendingReserves} />, element);
}

function StartBuyingMap(element, props) {
	props = _preprocess_querysets(props);
	ReactDOM.render(<BuyingMap room={props.rooms[0]} prices={props.prices} />, element);
}

function StartSeatPricePickerList(element, props) {
	props = _preprocess_querysets(props);
	ReactDOM.render(<SeatPricePickerList entryStates={props.entryStates} prices={props.prices} fields={props.fields} fieldHeads={props.fieldHeads} />, element);
}
