/* requires payment-selector.js */

var generic_map = function(predicate, entries, table, prices, sala_filas, sala_columnas, selects_container, total_output) {
	let map = new EntryMap(entries, sala_columnas, sala_filas, table);
	let setCheckBox = function(cb, b) {
		cb.disabled = !b;
		cb.readOnly = !b;
	}
	for (let i = 0; i < map.entradas.length; i++)
		setCheckBox(map.inputs[i], predicate(map.entradas[i]));

	let payment_selector = new PaymentSelector(prices, [], total_output);
	let updatePrices = function(checkbox) {
		let i = map.inputs.indexOf(checkbox);
		if (checkbox.checked == true) {
			let tr = document.createElement('tr');
			tr._checkbox = checkbox;
			let td_a = document.createElement('td');
			td_a.appendChild(document.createTextNode(map.entradas[i].butaca_str));
			let td_b = document.createElement('td');
			let select = document.createElement('select');
			select.name = 'precio';
			let hidden = document.createElement('input')
			hidden.type = 'hidden';
			hidden.name = 'entrada';
			hidden.value = map.entradas[i].id_entrada;
			td_b.appendChild(hidden);
			td_b.appendChild(select);
			tr.appendChild(td_a);
			tr.appendChild(td_b);
			selects_container.appendChild(tr);
			payment_selector.addSelect(select);
		}
		else {
			let children = Array.from(selects_container.children);
			let tr = children.find(function(c) { return c.tagName == 'TR' && c._checkbox === checkbox});
			let select = tr.querySelector('select');
			payment_selector.removeSelect(select);
			selects_container.removeChild(tr);
		}
	}
	for (let cb of map.inputs)
		cb.onclick = function(ev) { updatePrices(cb) };

	return map;
}

function BuyingMap(entries, table, prices, sala_filas, sala_columnas, selects_container, total_output) {
	return generic_map(function(entry) {
		return (entry.estado==='D' || entry.estado==='L') &&
			(entry.butaca_disponibilidad==='N' || entry.butaca_disponibilidad==='NR');
	}, entries, table, prices, sala_filas, sala_columnas, selects_container, total_output);
}

function SellingMap(entries, table, prices, sala_filas, sala_columnas, selects_container, total_output) {
	return generic_map(function(entry) {
		return (entry.estado==='D' || entry.estado==='L' || entry.estado=='R') &&
			(entry.butaca_disponibilidad==='N' || entry.butaca_disponibilidad==='NR');
	}, entries, table, prices, sala_filas, sala_columnas, selects_container, total_output);
}
