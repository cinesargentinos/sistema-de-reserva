'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var int2letters = function int2letters(x, charset) {
	charset = charset || 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	var s = [x % charset.length];
	x = Math.floor(x / charset.length);
	while (x > 0) {
		s.splice(0, 0, Math.max(0, x % (charset.length + 1) - 1));
		x = Math.floor(x / (charset.length + 1));
	}
	s = s.map(function (idx) {
		return charset[idx];
	});
	return s.join('');
};

var Butaca = function () {
	function Butaca(data) {
		_classCallCheck(this, Butaca);

		this.pk = data.pk;
		this.fields = data.fields;
	}

	_createClass(Butaca, [{
		key: 'toString',
		value: function toString() {
			return int2letters(this.fields.fila - 1) + '-' + this.fields.columna;
		}
	}]);

	return Butaca;
}();

var EstadoEntrada = function () {
	function EstadoEntrada(data) {
		_classCallCheck(this, EstadoEntrada);

		this.pk = data.pk;
		this.fields = data.fields;
	}

	_createClass(EstadoEntrada, [{
		key: 'get_estado_display',
		value: function get_estado_display() {
			return {
				'R': 'Reservada',
				'L': 'Libre',
				'D': 'Disponible',
				'P': 'Paga'
			}[this.fields.estado];
		}
	}]);

	return EstadoEntrada;
}();
/*Array.prototype.binarySearch = function(compare_to) {
	let left = 0;
	let right = this.length;
	while(left <= right) {
		let pivot_key = Math.floor((left+right)/2)
		let v = compare_to(this[pivot_key]);
		if (v < 0)
			right = pivot_key-1;
		else if (v > 0)
			left = pivot_key+1;
		else
			return this[pivot_key]
	}
	return null;
}*/

function Seat(props) {
	var eState = '\xA0';
	var input = null;
	if (props.entryState.fields.entrada.fields.butaca.fields.disponibilidad != 'V') {
		eState = props.entryState.fields.estado;
		input = React.createElement('input', { type: 'checkbox', name: 'entradas',
			value: props.entryState.pk, disabled: props.disabled,
			readOnly: props.disabled, onClick: props.onClick });
	}
	return React.createElement(
		'td',
		{ className: 'entrada' },
		eState,
		input
	);
}

var GenericEntryMap = function (_React$Component) {
	_inherits(GenericEntryMap, _React$Component);

	function GenericEntryMap(props) {
		_classCallCheck(this, GenericEntryMap);

		return _possibleConstructorReturn(this, (GenericEntryMap.__proto__ || Object.getPrototypeOf(GenericEntryMap)).call(this, props));
	}

	_createClass(GenericEntryMap, [{
		key: 'renderSeat',
		value: function renderSeat(i) {
			var _this2 = this;

			var es = this.props.room.fields.butaca_set[i].fields.entrada_set[0].fields.estadoentrada_set[0];
			if (!this.props.onSeatClick) var onSeatClick = function onSeatClick() {};else var onSeatClick = function onSeatClick(ev) {
				return _this2.props.onSeatClick(i, ev, es);
			};
			return React.createElement(Seat, { key: i, entryState: es, disabled: this.props.disabledSeats[i], onClick: onSeatClick });
		}
	}, {
		key: 'shouldComponentUpdate',
		value: function shouldComponentUpdate(nextProps, nextState) {
			return this.props.disabledSeats !== nextProps.disabledSeats;
		}
	}, {
		key: 'render',
		value: function render() {
			var _this3 = this;

			var room = this.props.room;
			return React.createElement(
				'table',
				null,
				React.createElement(
					'thead',
					null,
					React.createElement(
						'tr',
						null,
						React.createElement('td', null),
						[].concat(_toConsumableArray(Array(room.fields.cant_columnas).keys())).map(function (c) {
							return React.createElement(
								'td',
								{ key: c },
								c + 1
							);
						})
					)
				),
				React.createElement(
					'tbody',
					null,
					[].concat(_toConsumableArray(Array(room.fields.cant_filas).keys())).map(function (row) {
						return React.createElement(
							'tr',
							{ key: row },
							React.createElement(
								'td',
								null,
								int2letters(row)
							),
							[].concat(_toConsumableArray(Array(room.fields.cant_columnas).keys())).map(function (col) {
								return _this3.renderSeat(row * _this3.props.room.fields.cant_columnas + col);
							})
						);
					})
				)
			);
		}
	}]);

	return GenericEntryMap;
}(React.Component);

function ReserveCounter(props) {
	if (props.reservesLeft < 0) return React.createElement(
		'p',
		null,
		'Pod\xE9s reservar todas las entradas que quieras'
	);else if (props.reservesLeft == 0) return React.createElement(
		'p',
		null,
		'No pod\xE9s reservar m\xE1s entradas'
	);else if (props.reservesLeft == 1) return React.createElement(
		'p',
		null,
		'Pod\xE9s reservar una entrada m\xE1s'
	);else if (props.reservesLeft > 0) return React.createElement(
		'p',
		null,
		'Pod\xE9s reservar ',
		props.reservesLeft,
		' entradas m\xE1s'
	);
}

var ReservesMap = function (_React$Component2) {
	_inherits(ReservesMap, _React$Component2);

	function ReservesMap(props) {
		_classCallCheck(this, ReservesMap);

		var _this4 = _possibleConstructorReturn(this, (ReservesMap.__proto__ || Object.getPrototypeOf(ReservesMap)).call(this, props));

		_this4.state = {
			'reserved': 0,
			'checkedEntryStates': []
		};
		_this4.onSeatClick = _this4.onSeatClick.bind(_this4);
		return _this4;
	}

	_createClass(ReservesMap, [{
		key: 'onSeatClick',
		value: function onSeatClick(i, ev, entryState) {
			var checkedEntryStates = this.state.checkedEntryStates.slice();
			checkedEntryStates[i] = !checkedEntryStates[i];
			if (checkedEntryStates[i]) this.setState({ 'reserved': this.state.reserved + 1, 'checkedEntryStates': checkedEntryStates });else this.setState({ 'reserved': this.state.reserved - 1, 'checkedEntryStates': checkedEntryStates });
		}
	}, {
		key: 'reservesLeft',
		value: function reservesLeft() {
			if (this.props.maxReserves == null || this.props.maxReserves == -1) return -1;else return this.props.maxReserves - this.props.pendingReserves - this.state.reserved;
		}
	}, {
		key: 'isSeatDisabled',
		value: function isSeatDisabled(i, ev) {
			return (this.props.room.fields.butaca_set[i].fields.entrada_set[0].fields.estadoentrada_set[0].fields.estado != 'D' || this.reservesLeft() == 0) && !this.state.checkedEntryStates[i];
		}
	}, {
		key: 'render',
		value: function render() {
			var _this5 = this;

			var room_size = this.props.room.fields.cant_filas * this.props.room.fields.cant_columnas;
			return React.createElement(
				'div',
				null,
				React.createElement(GenericEntryMap, {
					room: this.props.room,
					onSeatClick: this.onSeatClick,
					disabledSeats: [].concat(_toConsumableArray(Array(room_size).keys())).map(function (i) {
						return _this5.isSeatDisabled(i);
					})
				}),
				React.createElement(ReserveCounter, { reservesLeft: this.reservesLeft() })
			);
		}
	}]);

	return ReservesMap;
}(React.Component);

function SeatPricePicker(props) {
	var onChange = function onChange(ev) {
		return props.onChange(ev, props.prices.find(function (o) {
			return o.pk == ev.target.value;
		}));
	};
	return React.createElement(
		'tr',
		null,
		props.fields.map(function (f, i) {
			return React.createElement(
				'td',
				{ key: i },
				f(props.entryState)
			);
		}),
		React.createElement(
			'td',
			null,
			React.createElement(
				'select',
				{ name: 'precio', onChange: onChange },
				props.prices.map(function (p) {
					return React.createElement(
						'option',
						{ key: p.pk, value: p.pk },
						p.fields.tipo_precio.fields.nombre,
						' $',
						p.fields.monto
					);
				})
			)
		)
	);
}

var SeatPricePickerList = function (_React$Component3) {
	_inherits(SeatPricePickerList, _React$Component3);

	function SeatPricePickerList(props) {
		_classCallCheck(this, SeatPricePickerList);

		var _this6 = _possibleConstructorReturn(this, (SeatPricePickerList.__proto__ || Object.getPrototypeOf(SeatPricePickerList)).call(this, props));

		_this6.state = {
			'selectedPrices': Array(_this6.props.entryStates.length).fill(_this6.props.prices[0])
		};
		_this6.priceChanged = _this6.priceChanged.bind(_this6);
		return _this6;
	}

	_createClass(SeatPricePickerList, [{
		key: 'componentWillReceiveProps',
		value: function componentWillReceiveProps(nextProps) {
			var fillValue = this.props.prices[0];
			var _ref = [this.state.selectedPrices.length, nextProps.entryStates.length],
			    a = _ref[0],
			    b = _ref[1];

			if (a < b) this.setState({ 'selectedPrices': this.state.selectedPrices.concat(Array(b - a).fill(fillValue)) });else if (b < a) this.setState({ 'selectedPrices': this.state.selectedPrices.slice(0, b) });
		}
	}, {
		key: 'priceChanged',
		value: function priceChanged(i, ev, price) {
			var selectedPrices = this.state.selectedPrices.slice();
			selectedPrices[i] = price;
			this.setState({ 'selectedPrices': selectedPrices });
		}
	}, {
		key: 'getTotal',
		value: function getTotal() {
			return this.state.selectedPrices.reduce(function (x, p) {
				return x + parseFloat(p.fields.monto);
			}, 0);
		}
	}, {
		key: 'render',
		value: function render() {
			var _this7 = this;

			return React.createElement(
				'table',
				{ id: 'seleccion-precio' },
				React.createElement(
					'thead',
					null,
					React.createElement(
						'tr',
						null,
						this.props.fieldHeads.map(function (fh, i) {
							return React.createElement(
								'td',
								{ key: i },
								fh
							);
						}),
						React.createElement(
							'td',
							null,
							'Tipo de Entrada'
						)
					)
				),
				React.createElement(
					'tbody',
					null,
					this.state.selectedPrices.map(function (p, i) {
						return React.createElement(SeatPricePicker, { key: i, entryState: _this7.props.entryStates[i],
							prices: _this7.props.prices, fields: _this7.props.fields,
							onChange: function onChange(ev, p) {
								return _this7.priceChanged(i, ev, p);
							}, value: p });
					})
				),
				React.createElement(
					'tfoot',
					null,
					React.createElement(
						'tr',
						null,
						this.props.fields.slice(-2).map(function (k) {
							return React.createElement('td', { key: k });
						}),
						React.createElement(
							'td',
							null,
							'Subtotal'
						),
						React.createElement(
							'td',
							null,
							'$',
							this.getTotal()
						)
					)
				)
			);
		}
	}]);

	return SeatPricePickerList;
}(React.Component);

var BuyingMap = function (_React$Component4) {
	_inherits(BuyingMap, _React$Component4);

	function BuyingMap(props) {
		_classCallCheck(this, BuyingMap);

		var _this8 = _possibleConstructorReturn(this, (BuyingMap.__proto__ || Object.getPrototypeOf(BuyingMap)).call(this, props));

		_this8.state = { 'checkedEntryStates': [] };
		_this8.onSeatClick = _this8.onSeatClick.bind(_this8);
		return _this8;
	}

	_createClass(BuyingMap, [{
		key: 'onSeatClick',
		value: function onSeatClick(i, ev, entryState) {
			var checkedEntryStates = this.state.checkedEntryStates.slice();
			checkedEntryStates[i] = !checkedEntryStates[i];
			if (checkedEntryStates[i]) this.setState({ 'checkedEntryStates': checkedEntryStates });else this.setState({ 'checkedEntryStates': checkedEntryStates });
		}
	}, {
		key: 'onSeatClick',
		value: function onSeatClick(i, ev, entryState) {
			var ss = this.state.checkedEntryStates;
			if (ev.target.checked) ss = ss.concat([entryState]);else ss = ss.filter(function (s) {
				return s.pk != entryState.pk;
			});
			this.setState({ 'checkedEntryStates': ss });
		}
	}, {
		key: 'isSeatDisabled',
		value: function isSeatDisabled(i) {
			var ee = this.props.room.fields.butaca_set[i].fields.entrada_set[0].fields.estadoentrada_set[0];
			return ee.fields.estado == 'R' || ee.fields.estado == 'P';
		}
	}, {
		key: 'render',
		value: function render() {
			var _this9 = this;

			var room_size = this.props.room.fields.cant_filas * this.props.room.fields.cant_columnas;
			return React.createElement(
				'div',
				null,
				React.createElement(GenericEntryMap, {
					room: this.props.room,
					onSeatClick: this.onSeatClick,
					disabledSeats: [].concat(_toConsumableArray(Array(room_size).keys())).map(function (i) {
						return _this9.isSeatDisabled(i);
					}) }),
				React.createElement(SeatPricePickerList, {
					fieldHeads: ['Butaca'],
					fields: [function (es) {
						return es.fields.entrada.fields.butaca.toString();
					}],
					entryStates: this.state.checkedEntryStates,
					prices: this.props.prices })
			);
		}
	}]);

	return BuyingMap;
}(React.Component);

/*function StartMap(element, room, seats, entries, entryStates) {
	console.log(element);
	ReactDOM.render(<GenericEntryMap room={room} seats={seats} entries={entries} entryStates={entryStates}  />, element);
}*/

function _preprocess_querysets(props) {
	var relate = function relate(set1, relationField, set2, relatedField) {
		var _iteratorNormalCompletion = true;
		var _didIteratorError = false;
		var _iteratorError = undefined;

		try {
			var _loop = function _loop() {
				var object = _step.value;

				var relatedObject = set2.find(function (obj) {
					return obj.pk == object.fields[relationField];
				});
				object.fields[relationField] = relatedObject;
				if (relatedField) {
					var rf = relatedObject.fields[relatedField] || [];
					rf.push(object);
					relatedObject.fields[relatedField] = rf;
				}
			};

			for (var _iterator = set1[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
				_loop();
			}
		} catch (err) {
			_didIteratorError = true;
			_iteratorError = err;
		} finally {
			try {
				if (!_iteratorNormalCompletion && _iterator.return) {
					_iterator.return();
				}
			} finally {
				if (_didIteratorError) {
					throw _iteratorError;
				}
			}
		}
	};

	if (props.seats) {
		props.seats = props.seats.map(function (s) {
			return new Butaca(s);
		});
		if (props.rooms) {
			(function () {
				var seatSortKey = function seatSortKey(seat) {
					return seat.fields.fila * seat.fields.sala.fields.cant_columnas + seat.fields.columna;
				};
				relate(props.seats, 'sala', props.rooms, 'butaca_set');
				var _iteratorNormalCompletion2 = true;
				var _didIteratorError2 = false;
				var _iteratorError2 = undefined;

				try {
					for (var _iterator2 = props.rooms[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
						var r = _step2.value;

						r.fields.butaca_set = r.fields.butaca_set.sort(function (a, b) {
							return seatSortKey(a) - seatSortKey(b);
						});
					}
				} catch (err) {
					_didIteratorError2 = true;
					_iteratorError2 = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion2 && _iterator2.return) {
							_iterator2.return();
						}
					} finally {
						if (_didIteratorError2) {
							throw _iteratorError2;
						}
					}
				}

				props.seats = props.seats.sort(function (a, b) {
					return seatSortKey(a) - seatSortKey(b);
				});
			})();
		}
	}
	if (props.seats && props.entries) relate(props.entries, 'butaca', props.seats, 'entrada_set');
	if (props.entryStates) {
		props.entryStates = props.entryStates.map(function (data) {
			return new EstadoEntrada(data);
		});
		if (props.entries) relate(props.entryStates, 'entrada', props.entries, 'estadoentrada_set');
	}
	if (props.priceTypes && props.prices) relate(props.prices, 'tipo_precio', props.priceTypes, 'precio_set');

	return props;
}

function StartReservesMap(element, props) {
	props = _preprocess_querysets(props);
	ReactDOM.render(React.createElement(ReservesMap, { room: props.rooms[0], maxReserves: props.maxReserves, pendingReserves: props.pendingReserves }), element);
}

function StartBuyingMap(element, props) {
	props = _preprocess_querysets(props);
	ReactDOM.render(React.createElement(BuyingMap, { room: props.rooms[0], prices: props.prices }), element);
}

function StartSeatPricePickerList(element, props) {
	props = _preprocess_querysets(props);
	ReactDOM.render(React.createElement(SeatPricePickerList, { entryStates: props.entryStates, prices: props.prices, fields: props.fields, fieldHeads: props.fieldHeads }), element);
}