
/* requires mapa.js */

function ReservesMap(entries, table, output, reserves_max, reserves_pending, sala_filas, sala_columnas) {
	let can_reserve = function() { return reserves_max== -1 || reserves_max-reserves_pending > 0; };
	let predicate = function(entry) { return entry.estado==='D' && entry.butaca_disponibilidad==='N' && can_reserve(); };
	let map = new EntryMap(entries, sala_columnas, sala_filas, table);
	let setCheckBox = function (cb, b) {
		cb.disabled = !b;
		cb.readOnly = !b;
	}
	let updateCheckBox = function(i) {
		setCheckBox(map.inputs[i], predicate(map.entradas[i]));
	}
	for (let i = 0; i < map.entradas.length; i++)
		updateCheckBox(i);

	function updateReservationCount() {
		let reserves_left = reserves_max - map.inputs.reduce(function(a,b) { return a + b.checked; }, 0) - reserves_pending;
		if (reserves_max == -1) {
			output.innerHTML = 'Podés reservar todas las entradas que quieras';
			for (let i = 0; i < map.entradas.length; i++)
				updateCheckBox(i);
		}
		else if (reserves_left <= 0) {
			if (reserves_max == 0)
				output.innerHTML = 'No podés reservar hasta registrarte o ingresar';
			else
				output.innerHTML = 'No podés reservar más entradas';
			for (let cb of map.inputs)
				if (cb.checked == false)
					setCheckBox(cb, false);
		}
		else if (reserves_left > 0) {
			output.innerHTML = 'Podés reservar ' + reserves_left + ' entrada' + (reserves_left>1 ? 's' : '');
			for (let i = 0; i < map.entradas.length; i++)
				updateCheckBox(i);
		}
	}
	for (let cb of map.inputs)
		cb.onclick = updateReservationCount;
	updateReservationCount();
	return map;
}
