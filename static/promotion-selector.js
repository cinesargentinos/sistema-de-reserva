
function PromotionSelector(prices, payment_selects, total_output, promotions, promotions_output) {
	var ps = new PaymentSelector(prices, payment_selects, total_output);
	let promotionsById = {};
	for (let p of promotions)
		promotionsById[p.id] = p;

	let selects = [];
	let used_promotables = [];

	let usable_entries = function() {
		let candidates = ps.getSelectedPromotableEntries();
		let used = used_promotables.slice();
		for (let u in used) {
			for (let i = 0; i < candidates.length; i++) {
				if (c.id == used[i].id) {
					candidates.splice(i, 1)
					break;
				}
			}
		}
		return candidates;
	}

	let create_promo_select = function() {
		let usable = usable_entries();
		var s = document.createElement('select');
		for (let p of promotions) {
			let option = document.createElement('option');
			option.value = p.id;
			option.appendChild(document.createTextNode(p.str));
			s.appendChild(option);
		}
		return s;
	}

	let create_price_select = function(promotion) {
		let usable = usable_entries();
		let counts_by_id = [];
		let usable_set = [];
		for (let u of usable) {
			if (counts_by_id[u.id] == null)
				counts_by_id[u.id] = 0;
			counts_by_id[u.id] += 1;
			if (!usable_set.find(function(x) { return x.id==u.id })) {
				usable_set.push(u);
			}
		}
		var s = document.createElement('select');
		for (let o of usable_set) {
			let option = document.createElement('option');
			option.value = o.id;
			option.appendChild(document.createTextNode(o.str));
			s.appendChild(option);
		}
		return s;
	}

	let create_row = function() {
		let tr = document.createElement('tr');
		let td1 = document.createElement('td');
		let s1 = create_promo_select();
		//let td2 = document.createElement('td');
		td1.appendChild(s1);
		tr.appendChild(td1);
		return tr;
	}

}
