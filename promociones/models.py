from django.db import models

# Create your models here.

from django.core import validators
from django.core.exceptions import ValidationError

# Create your models here.

class Promocion(models.Model):
	class Meta:
		ordering = ['-desde']
		verbose_name_plural = 'Promociones'
	# Procentaje a descontar sobre el precio general de entrada	[NAP]==False
	Porcentaje, P_TEXT = ("Porcentaje", "Porcentaje de Descuento")
	# Monto fijo en $$ a descontar del valor del precio de la entrada [NAP]==False
	MontoFijo, M_TEXT = ("MontoFijo", "Monto Fijo de Descuento")
	# Valor directamente de la entrada en $.
	# Con una promo de este tipo la idea es tu entrada sale $ xxx
	ValorEntrada, V_TEXT = ("ValorEntrada", "Valor Entrada")

	BONIFICACION_CHOICES = (
		(Porcentaje, P_TEXT),
		(MontoFijo, M_TEXT),
		(ValorEntrada, V_TEXT),
	)

	nombre = models.CharField("Nombre de Promocion",max_length=100,  null=False, blank=False)
	tipo_bonificacion = models.CharField("Tipo de Bonificación", max_length=26, choices=BONIFICACION_CHOICES, blank=False, null=False)
	valor_bonificacion = models.DecimalField(max_digits=10,decimal_places=2, validators=[validators.MinValueValidator(0)])
	cantidad_entradas_min = models.IntegerField(default=1, blank=False, null=False)
	desde = models.DateTimeField(null=False, blank=False)
	hasta = models.DateTimeField(null=True, blank=True)
	vigente = models.BooleanField(default=True, null=False)

	# Condiciones TODO
	# Pelicula
	# Tarjeta
	# Tipo de video
	# Etc...

	def __str__(self):
		return '%s %s  (%.2f)' % (
			self.nombre,
			self.tipo_bonificacion,
			self.valor_bonificacion
		)
