const gulp = require('gulp');
const babel = require('gulp-babel');

function swallowError(error) {
	console.log(error.stack);
	this.emit('end');
}
function scripts() {
	return gulp.src('static/src-js/*.js')
		.pipe(babel({presets: ['es2015', 'react']}))
		.on('error', swallowError)
		.pipe(gulp.dest('static/sitema-de-reserva/js'))
}

function watch_scripts() {
	gulp.watch('static/src-js/*.js', scripts)
}

exports.build = scripts
exports.default = scripts
exports.watch = watch_scripts
