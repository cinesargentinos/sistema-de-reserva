dropdb -U postgres cinesargentinos &&\
createdb -U postgres cinesargentinos &&\
python3 migrar.py &&\
python3 manage.py loaddata salas peliculas funciones reservas simulador usuarios promociones &&\
python3 manage.py createsuperuser &&\
python3 manage.py runserver
