from django.contrib import admin

from .models import Cuenta,Acreditacion,Pago
# Register your models here.

bank_site = admin.AdminSite(name='banco')

@admin.register(Cuenta, site=bank_site)
class CuentaAdmin(admin.ModelAdmin):
	list_display = ('numero_cuenta', 'titular', 'vencimiento', 'saldo',)

@admin.register(Acreditacion, site=bank_site)
class AcreditacionAdmin(admin.ModelAdmin):
	pass

@admin.register(Pago, site=bank_site)
class AcreditacionAdmin(admin.ModelAdmin):
	pass
