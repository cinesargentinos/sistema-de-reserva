
from django.conf.urls import include, url
from django.contrib.auth.views import login,logout

from . import views

urlpatterns = [
    url(r'^registro/$', views.RegistrationView.as_view(), name='accounts-register'),
    url(r'^login/$', login, name='accounts-login'),
    url(r'^logout/$', logout, {'next_page':'inicio'}, name='accounts-logout'),
    url(r'^reservas/$', views.ReservationsView.as_view(), name='accounts-reservations'),
    url(r'^cancelar/$', views.reservations_cancel_view, name='accounts-cancel')
]
