from django.db import models
from django.core.validators import RegexValidator, MinValueValidator
from django.contrib.auth.models import AbstractUser, Group
from django.db.models.signals import post_save
from django.dispatch import receiver

class Persona(models.Model):
	tipo_dni = models.CharField('Tipo DNI', max_length=10, choices=(
		('DNI', 'Documento Nacional de Identificación'),
	), null=False, blank=False)
	nro_dni = models.IntegerField(
		'Numero DNI', primary_key=True,
		validators=(MinValueValidator(0),)
	)
	nombre = models.CharField(null=False, blank=True, max_length=50)
	apellido = models.CharField(null=False, blank=True, max_length=50)
	fecha_nac = models.DateField('Fecha de Nacimiento', null=False)
	sexo = models.CharField(max_length=1, choices=(
		('F','Femenino'),
		('M', 'Masculino'),
		('O', 'Otro')
	), null=False, blank=False)
	direccion = models.CharField(null=False, blank=True, max_length=50)
	telefono = models.CharField(
		max_length=15,
		validators=(
			RegexValidator(regex=r'^\+\d{9,15}$', message='Formato incorrecto'),
		),
		help_text='Número telefónico en formato +999999999 con 9 a 15 digitos'
	)
	def __str__(self):
		return str('%s, %s - %s: %d' % (self.apellido, self.nombre, self.tipo_dni, self.nro_dni))

class LimiteReserva(models.Model):
	nombre = models.CharField('Nombre de la restriccion', max_length=20, null=False, blank=False, unique=True)
	max_reserva = models.IntegerField(
		'Cantidad maxima de reservas pendientes',
		null=False, blank=False, default=2,
		validators=(MinValueValidator(0),)
	)
	def __str__(self):
		return '%s (máx: %d)' % (self.nombre, self.max_reserva)

class User(AbstractUser):
	persona = models.ForeignKey(Persona, null=True, blank=True, on_delete=models.CASCADE)
	limite_reserva = models.ForeignKey(LimiteReserva, null=True, blank=True, verbose_name='Limites de reservas')

	class Meta:
		db_table = 'auth_user'
		verbose_name_plural = 'Usuarios'
		verbose_name = 'Usuario'
