
from .models import Persona,User
from django import forms
from django.contrib.auth.forms import UserCreationForm

class PersonaForm(forms.ModelForm):
	def __init__(self, *args, **kwargs):
		super(PersonaForm, self).__init__(*args, **kwargs)
		self.fields['fecha_nac'].widget.attrs['class'] = 'datepicker'
	class Meta:
		model = Persona
		fields = '__all__'

class RegistroForm(UserCreationForm):
	email = forms.EmailField(label='Dirección de correo electronico', required=True)
	class Meta:
		model = User
		fields = ('username', 'email', 'password1', 'password2')
	def save(self, commit=True):
		user = super(RegistroForm, self).save(commit=False)
		user.email = self.cleaned_data['email']
		if commit:
			user.save()
		return user
