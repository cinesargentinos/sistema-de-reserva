
from django.core.validators import MinValueValidator
from django.utils import timezone
from django.db import models

class Idioma(models.Model):
    nombre = models.CharField(unique=True, max_length=30)

    def __str__(self):
        return self.nombre

class TipoVideo(models.Model):
	class Meta:
		verbose_name_plural = 'Tipos de video'

	nombre = models.CharField(unique=True,blank=False, null=False, max_length=30)
	descripcion = models.TextField(blank=True, null=True)

	def __str__(self):
		return self.nombre

class Genero(models.Model):
    nombre = models.CharField(unique=True, max_length=25)
    descripcion = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.nombre

class Pelicula(models.Model):
	# Clasificacion Argentina de peliculas
	APT, APT_TEXT = ("APT", "Apta para Todo Público")
	MAYOR_13, MAYOR_13_TEXT =  ("+13", "Apta mayores de 13 años")
	MAYOR_16, MAYOR_16_TEXT =  ("+16", "Apta mayores de 16 años")
	MAYOR_18, MAYOR_18_TEXT =  ("+18", "Apta mayores de 18 años")

	CLASIFICACION_CHOICES = (
		(APT, APT_TEXT),
		(MAYOR_13, MAYOR_13_TEXT),
		(MAYOR_16, MAYOR_16_TEXT),
		(MAYOR_18, MAYOR_18_TEXT),
	)

	edicion_de = models.ForeignKey('self', blank=True, null=True)
	# Edicion_de vacio:
	titulo_original = models.CharField(max_length=100, blank=True, null=True)
	sinopsis = models.TextField(max_length=600, blank=True, null=True)
	duracion = models.DurationField("Duración de la película", blank=False, null=False, help_text='En formato hh:mm:ss. Ejemplo: 02:55:00')
	clasificacion = models.CharField("Clasificación", max_length=26, choices=CLASIFICACION_CHOICES, blank=True, null=True)
	genero = models.ManyToManyField(Genero, blank=True)
	disponible = models.BooleanField("Pelicula Disponible para crear nuevas Funciones")

	# Atributos que cambian de una edicion a otra
	titulo = models.CharField("Titulo a mostrar",max_length=100,  null=False, blank=False)
	isan = models.IntegerField(unique=True, validators=(
		(MinValueValidator(0)),

	))
	audio = models.ForeignKey(Idioma, null=False, related_name='audio')
	subtitulo = models.ForeignKey(Idioma, null=True, blank=True, related_name='subtitulo')
	tipo_video = models.ForeignKey(TipoVideo, null=False)

	fecha_estreno = models.DateField("Fecha de Estreno", blank=True, null=True)
	poster_icono = models.ImageField("Poster Cartelera", null=False, upload_to='peliculas/iconos')
	poster_fondo = models.ImageField("Poster Fondo", null=False, upload_to='peliculas/fondos')

	def proximas_funciones(self):
		return self.funcion_set.filter(fecha__gte=timezone.now())

	def __str__(self):
		#return self.titulo
		return '"%s" - %s - %s' % (self.titulo, self.audio, self.tipo_video)
