from django import template

register = template.Library()

@register.filter
def td_minutes_part(td):
	return td.seconds//60 % 60

@register.filter
def td_minutes_total(td):
	return td.seconds//60

@register.filter
def td_hours_part(td):
	return td.seconds//3600%24

@register.filter
def td_hours_total(td):
	return td.seconds//3600
