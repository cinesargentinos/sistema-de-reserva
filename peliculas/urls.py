from django.conf.urls import url, include
from . import views

urlpatterns = [
    url(r'ver/(?P<id_pelicula>[0-9]+)$', views.VerPeliculaView.as_view(), name='ver-pelicula'),
    url(r'raw/(?P<id_pelicula>[0-9]+)$', views.raw_pelicula, name='pelicula-raw-get')
]
