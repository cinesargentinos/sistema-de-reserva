
from django.conf.urls import include, url

from . import views

urlpatterns = [
    url(r'^map/(\d+)/$', views.seat_map, name='seat-map'),
]
