
from datetime import timedelta

from django.db.models import Q
from django.core import validators
from django.core.exceptions import ValidationError
from django.db import models
from django.utils import timezone

from salas.models import Sala
from peliculas.models import Pelicula
from promociones.models import Promocion

class TipoPrecio(models.Model):
	class Meta:
		verbose_name_plural = 'Tipos de precio'
	nombre = models.CharField(max_length=30)
	nap = models.BooleanField("No Aplica Promoción")
	descripcion = models.CharField(max_length=300)
	def __str__(self):
		return self.nombre

class Precio(models.Model):
	class Meta:
		ordering = ['-desde']

	tipo_precio = models.ForeignKey(TipoPrecio, on_delete=models.CASCADE, null=False)
	desde = models.DateTimeField(null=False, blank=False)
	hasta = models.DateTimeField(null=True, blank=True)
	habilitado = models.BooleanField(default=True, null=False)
	monto = models.DecimalField(max_digits=10, decimal_places=2, validators=[validators.MinValueValidator(0),validators.MaxValueValidator(600)])

	def __str__(self):
		return '%s ($%.2f) %s' % (self.tipo_precio.nombre, self.monto, '[NAP]' if self.tipo_precio.nap else '')

	def esta_vigente(self):
		now = timezone.now()
		return now >= self.desde and (self.hasta is None or now <= self.hasta) and self.habilitado

class Funcion(models.Model):
	class Meta:
		verbose_name_plural = 'Funciones'
	sala = models.ForeignKey(Sala, on_delete=models.CASCADE, null=False)
	pelicula = models.ForeignKey(Pelicula, on_delete=models.CASCADE, null=False)
	fecha = models.DateTimeField('Fecha y hora de la funcion', null=False)
	precios = models.ManyToManyField(Precio)
	promociones = models.ManyToManyField(Promocion, blank=True)

	def fecha_fin(self):
		return self.fecha + self.pelicula.duracion

	def precios_vigentes(self):
		return list(self.precios.filter(Q(habilitado=True), Q(hasta=None) | Q(hasta__gte=timezone.now())))

	def clean(self):
		super().clean()
		super().clean_fields()
		for f in Funcion.objects.filter(sala=self.sala).exclude(pk=self.id).all():
			if not (f.fecha_fin() <= self.fecha or f.fecha >= self.fecha_fin()):
				raise ValidationError('La funcion se superpone con otra: %s' % f)

	def __str__(self):
		lt = timezone.localtime(self.fecha)
		return '%s en %s, el %s a las %s' % (self.pelicula, self.sala, lt.date(), lt.time())
