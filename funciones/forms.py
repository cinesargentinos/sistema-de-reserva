
from .models import Precio
from django import forms

class PrecioForm(forms.ModelForm):
	class Meta:
		model = Precio
		fields = '__all__'
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)

		try:
			self.fields['hasta'].widget.attrs['placeholder'] = 'Abierto'
		except KeyError:
			pass

		
