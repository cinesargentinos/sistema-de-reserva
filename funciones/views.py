from django.shortcuts import render
from . import models
from django.core.serializers import serialize
import json
# Create your views here.

def seat_map(request, funcid):
    f = models.Funcion.objects.get(pk=funcid)
    seats = serialize('json', f.sala.butaca_set.all())
    return render(request, 'mapa-butacas.html', {'seat_data': seats})
    
