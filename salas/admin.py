import json

from django.contrib import admin
from django import forms

from .models import Butaca, Sala

@admin.register(Sala)
class SalaAdmin(admin.ModelAdmin):
	change_form_template = 'admin/salas/change_sala.html'
	def change_view(self, request, object_id, form_url='', extra_context=None):
		extra_context = extra_context or {}
		extra_context['seats'] = json.dumps([
			{'id': b.pk, 'fila': b.fila, 'columna': b.columna, 'disponibilidad': b.disponibilidad}
			for b in
			Butaca.objects.filter(sala=object_id)
		])
		extra_context['sala'] = Sala.objects.get(pk=object_id)
		if (request.method == 'POST'):
			for butaca_id in request.POST.getlist('butacas'):
				butaca = Butaca.objects.get(pk=butaca_id)
				butaca.disponibilidad = request.POST.get('disponibilidad')
				butaca.save()
		return super().change_view(request, object_id, form_url, extra_context=extra_context)
