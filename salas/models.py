from django.db import models
from django.core import validators
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator, MinValueValidator
from string import ascii_uppercase
from math import log

# Create your models here.

class Sala(models.Model):
	nombre = models.CharField(max_length=20)
	direccion = models.CharField(max_length=60)
	cant_filas = models.IntegerField(validators=(
		(MinValueValidator(1)),
	))
	cant_columnas = models.IntegerField(validators=(
		(MinValueValidator(1)),
	))
	foto = models.ImageField("Foto de la sala", blank=True, null=True, upload_to='salas/fotos')
	def clean(self):
		self.clean_fields()
		if self.cant_filas * self.cant_columnas > 5000:
			raise ValidationError('El límite máximo de una sala son 5000 butacas')
	def __str__(self):
		return self.nombre

def _sala_crear_butacas(instance, created, raw, **kwargs):
	if not created or raw:
		return
	for y in range(0, instance.cant_filas):
		for x in range(0, instance.cant_columnas):
			b = Butaca.objects.get_or_create(sala=instance, fila=y+1, columna=x+1)
	instance.save()

models.signals.post_save.connect(_sala_crear_butacas, sender=Sala, dispatch_uid='_sala_crear_butacas')

def int2letters(x, charset=ascii_uppercase):
	s = [x % len(charset)]
	x //= len(charset)
	while x > 0:
		# if x|26*27, x%27 = -1, fix that with max()
		s = [max(0, x % (len(charset)+1) - 1)] + s
		x //= len(charset)+1
	return ''.join(charset[c] for c in s)

class Butaca(models.Model):
	V, N, NR, NC = 'V', 'N', 'NR', 'NC'
	DISPONIBILIDAD_CHOICES = (
		(V, 'Vacio'), # nada
		(N, 'Normal'), # reservar, comprar, vender
		(NR, 'No reservable'), # comprar, vender
		(NC, 'No comprable'), # comprar
	)

	sala = models.ForeignKey(Sala, on_delete=models.CASCADE)
	fila = models.IntegerField(
		validators = [validators.MinValueValidator(1)]
	)
	columna = models.IntegerField(
		validators = [validators.MinValueValidator(1)]
	)
	disponibilidad = models.CharField(max_length=2, choices=DISPONIBILIDAD_CHOICES, default=N, blank=False, null=False)

	class Meta:
		unique_together = (('sala', 'fila', 'columna'),)
		ordering = ('fila', 'columna')

	def __str__(self):
		return '%s: %s-%d' % (self.sala, int2letters(self.fila-1), self.columna)

	def clean(self):
		errors = {}
		if self.fila > self.sala.cant_filas:
			errors['fila'] = ValidationError('El numero de fila no puede exceder la capacidad de la sala.')
		if self.columna > self.sala.cant_columnas:
			errors['columna'] = ValidationError('El numero de columna no puede exceder la capacidad de la sala.')
		if errors:
			raise ValidationError(errors)
